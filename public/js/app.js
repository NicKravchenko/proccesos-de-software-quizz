/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/src/js/app.js":
/*!******************************!*\
  !*** ./assets/src/js/app.js ***!
  \******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _js_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ~/js/test */ "./assets/src/js/test.js");


/***/ }),

/***/ "./assets/src/js/test.js":
/*!*******************************!*\
  !*** ./assets/src/js/test.js ***!
  \*******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _content_preguntas_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../content/preguntas.json */ "./assets/src/content/preguntas.json");
/* eslint-disable no-constant-condition */

/* eslint-disable no-console */

/* eslint-disable no-loop-func */

const hiderButton = document.querySelector('.hider-button');
const questions = _content_preguntas_json__WEBPACK_IMPORTED_MODULE_0__;
const amountOfQuestions = Object.keys(questions).length;
const mainMenu = document.querySelector('.main-menu');
const playQuizz = document.querySelector('.main-menu__button');
const quizz = document.querySelector('.quizz');
const textContainer = document.querySelector('.quizz__text');
const answerButtons = document.querySelectorAll('.quizz__button');
const exitQuizz = document.querySelector('.quizz__exit-button');
const results = document.querySelector('.results');
const resultsText = document.querySelector('.results__correct-answers');
const exitResults = document.querySelector('.results__exit-button');
const delay = 800;
let questionId = 0;
let questionShowed = 0;
let usedQuestionId = [0];
let playerScore = 0;

function getRandomInt(max, summer) {
  return Math.floor(Math.random() * max + summer);
}

function firstHideSeconShow(firstWindow, secondWindow) {
  firstWindow.classList.add('hidden');
  firstWindow.classList.remove('flex');
  secondWindow.classList.add('flex');
  secondWindow.classList.remove('hidden');
}

function endGame() {
  firstHideSeconShow(quizz, results);
  resultsText.innerHTML = `Respondió ${playerScore.toString()} preguntas correctas!`;
}

function generateQuestionId() {
  do {
    questionId = getRandomInt(amountOfQuestions, 1);

    if (!usedQuestionId.includes(questionId)) {
      break;
    }
  } while (true);

  usedQuestionId.push(questionId);
}

function showAnswers(id) {
  const usedAnswerId = [];
  let answerId = 0;

  for (let i = 0; i < 4; i++) {
    do {
      answerId = getRandomInt(4, 0);

      if (!usedAnswerId.includes(answerId)) {
        break;
      }
    } while (true);

    usedAnswerId.push(answerId);
    answerButtons[i].innerHTML = questions[id].allAnswers[answerId];
  }
}

function showNewQuestion() {
  generateQuestionId();
  textContainer.innerHTML = questions[questionId].question;
  showAnswers(questionId);
  questionShowed += 1;

  if (questionShowed > 9) {
    endGame();
  }
}

function evaluateAnswer(id, button) {
  hiderButton.classList.remove('hidden');
  hiderButton.classList.add('flex');

  if (questions[id].answerCorrect === button.innerHTML) {
    playerScore += 1;
    button.classList.add('win');
    setTimeout(() => {
      button.classList.remove('win');
      hiderButton.classList.remove('flex');
      hiderButton.classList.add('hidden');
    }, delay);
  } else {
    button.classList.add('lose');
    setTimeout(() => {
      button.classList.remove('lose');
      hiderButton.classList.remove('flex');
      hiderButton.classList.add('hidden');
    }, delay);
  }
}

playQuizz.addEventListener('click', () => {
  firstHideSeconShow(mainMenu, quizz);
});

for (let i = 0; i < 4; i++) {
  answerButtons[i].addEventListener('click', () => {
    evaluateAnswer(questionId, answerButtons[i]);
    console.log(playerScore);
    setTimeout(() => {
      showNewQuestion();
    }, delay);
  });
}

exitQuizz.addEventListener('click', () => {
  firstHideSeconShow(quizz, mainMenu);
  usedQuestionId = [0]; // reset used question etc

  playerScore = 0;
  questionShowed = 0;
});
exitResults.addEventListener('click', () => {
  firstHideSeconShow(results, mainMenu);
  usedQuestionId = [0]; // reset used question etc

  playerScore = 0;
  questionShowed = 0;
});
showNewQuestion();

/***/ }),

/***/ "./assets/src/scss/app.scss":
/*!**********************************!*\
  !*** ./assets/src/scss/app.scss ***!
  \**********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./assets/src/content/preguntas.json":
/*!*******************************************!*\
  !*** ./assets/src/content/preguntas.json ***!
  \*******************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"1":{"question":"¿Cuál es el río más largo del mundo?","answerCorrect":"Río Amazonas","allAnswers":["Río Amazonas","Río Nilo","Río Mississippi","Rio Obi"]},"2":{"question":"¿Cuál es el edificio más alto del mundo?","answerCorrect":"El Burj Khalifa","allAnswers":["El Burj Khalifa","Torre de Shanghai","432 Park Avenue","Torre de eiffel"]},"3":{"question":"¿Dónde está Transilvania?","answerCorrect":"En Rumanía","allAnswers":["En Rumanía","En Polonia","En Czechia","En Eslovaquia"]},"4":{"question":"¿Cuál es la película con más Óscars de la historia del cine?","answerCorrect":"Todas las anteriores","allAnswers":["Todas las anteriores","Titanic","Ben-Hur","Señor de los anillos"]},"5":{"question":"¿Cuándo empezó la Revolución Rusa?","answerCorrect":"En 1917","allAnswers":["En 1917","En 1991","En 1902","En 1949"]},"6":{"question":"¿Cuál es el océano más grande del mundo?","answerCorrect":"El océano Pacífico","allAnswers":["El océano Pacífico","El océano Atlántico","El océano Índico","El océano glacial Ártico"]},"7":{"question":"¿Cuántos huesos tiene el cuerpo humano?","answerCorrect":"206 huesos","allAnswers":["206 huesos","173 huesos","312 huesos","225 huesos"]},"8":{"question":"¿Cuántas notas musicales existen?","answerCorrect":"Doce","allAnswers":["Doce","Siete","Cinco","Nueve"]},"9":{"question":"¿Dónde está la Universidad de Harvard?","answerCorrect":"Estados Unidos","allAnswers":["Estados Unidos","Inglatera","Francia","Rusia"]},"10":{"question":"¿Cuál es el mineral más duro del planeta?","answerCorrect":"El diamante","allAnswers":["El diamante","El carbono","El osmio","El titanio"]},"11":{"question":"¿Qué gas liberan las plantas al hacer la fotosíntesis?","answerCorrect":"Oxígeno","allAnswers":["Oxígeno","Ozono","Hidrógeno","Helio"]}}');

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/app": 0,
/******/ 			"css/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkassets_builder_configs"] = self["webpackChunkassets_builder_configs"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/app"], function() { return __webpack_require__("./assets/src/js/app.js"); })
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/app"], function() { return __webpack_require__("./assets/src/scss/app.scss"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;