/* eslint-disable no-constant-condition */
/* eslint-disable no-console */
/* eslint-disable no-loop-func */
import questionsJson from '../content/preguntas.json';

const hiderButton = document.querySelector('.hider-button');

const questions = questionsJson;
const amountOfQuestions = Object.keys(questions).length;

const mainMenu = document.querySelector('.main-menu');
const playQuizz = document.querySelector('.main-menu__button');

const quizz = document.querySelector('.quizz');

const textContainer = document.querySelector('.quizz__text');
const answerButtons = document.querySelectorAll('.quizz__button');

const exitQuizz = document.querySelector('.quizz__exit-button');

const results = document.querySelector('.results');
const resultsText = document.querySelector('.results__correct-answers');
const exitResults = document.querySelector('.results__exit-button');

const delay = 800;

let questionId = 0;
let questionShowed = 0;
let usedQuestionId = [0];
let playerScore = 0;

function getRandomInt(max, summer) {
  return Math.floor(Math.random() * max + summer);
}

function firstHideSeconShow(firstWindow, secondWindow) {
  firstWindow.classList.add('hidden');
  firstWindow.classList.remove('flex');

  secondWindow.classList.add('flex');
  secondWindow.classList.remove('hidden');
}

function endGame() {
  firstHideSeconShow(quizz, results);
  resultsText.innerHTML = `Respondió ${playerScore.toString()} preguntas correctas!`;
}

function generateQuestionId() {
  do {
    questionId = getRandomInt(amountOfQuestions, 1);

    if (!usedQuestionId.includes(questionId)) {
      break;
    }
  } while (true);

  usedQuestionId.push(questionId);
}

function showAnswers(id) {
  const usedAnswerId = [];
  let answerId = 0;

  for (let i = 0; i < 4; i++) {
    do {
      answerId = getRandomInt(4, 0);

      if (!usedAnswerId.includes(answerId)) {
        break;
      }
    } while (true);

    usedAnswerId.push(answerId);
    answerButtons[i].innerHTML = questions[id].allAnswers[answerId];
  }
}

function showNewQuestion() {
  generateQuestionId();
  textContainer.innerHTML = questions[questionId].question;

  showAnswers(questionId);

  questionShowed += 1;

  if (questionShowed > 9) {
    endGame();
  }
}

function evaluateAnswer(id, button) {
  hiderButton.classList.remove('hidden');
  hiderButton.classList.add('flex');

  if (questions[id].answerCorrect === button.innerHTML) {
    playerScore += 1;
    button.classList.add('win');

    setTimeout(() => {
      button.classList.remove('win');

      hiderButton.classList.remove('flex');
      hiderButton.classList.add('hidden');
    }, delay);
  } else {
    button.classList.add('lose');

    setTimeout(() => {
      button.classList.remove('lose');

      hiderButton.classList.remove('flex');
      hiderButton.classList.add('hidden');
    }, delay);
  }
}

playQuizz.addEventListener('click', () => {
  firstHideSeconShow(mainMenu, quizz);
});

for (let i = 0; i < 4; i++) {
  answerButtons[i].addEventListener('click', () => {
    evaluateAnswer(questionId, answerButtons[i]);
    console.log(playerScore);

    setTimeout(() => {
      showNewQuestion();
    }, delay);
  });
}

exitQuizz.addEventListener('click', () => {
  firstHideSeconShow(quizz, mainMenu);
  usedQuestionId = [0]; // reset used question etc
  playerScore = 0;
  questionShowed = 0;
});

exitResults.addEventListener('click', () => {
  firstHideSeconShow(results, mainMenu);
  usedQuestionId = [0]; // reset used question etc
  playerScore = 0;
  questionShowed = 0;
});

showNewQuestion();
