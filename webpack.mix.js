const mix = require('laravel-mix');
const config = require('./webpack.config');

mix
  .webpackConfig(config)
  .js('assets/src/js/app.js', 'js')
  .sass('assets/src/scss/app.scss', 'css')
  .setPublicPath('dist')
  .version();

mix.babelConfig({
  presets: ['@babel/preset-env'],
});
